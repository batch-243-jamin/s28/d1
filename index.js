// CRUD Operations
/*
	-CRUD operations are the heart of any backend application.
	-Mastering the CRUD operations is essential for any developer.
	-This helps in building character and increasing exposure to logical statements that will help us manipulate.
	-Mastering the CRUP operations of any languages makes us a valueble developer and makes the work easier for us to deal with huge amounts of information.
*/

// [Sections] Inserting document (Create)

	// Insert one document
		/*
			-since MongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
			-mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand its code.
			Syntax:
				db.collectionName.insertOne({object});
			Javascript:
				object.object.method({object});
		*/

		db.users.insert({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "12345678",
				email: "janedoe@gmail.com",	
			},
			courses: ["CSS", "JavaScript", "Python"],
			department: "none"
		})

		// Insert Many
		/*
			-Syntax
			db.collectionName.insertMany([{ObjectA}, {ObjectB}])
		*/

		db.users.insertMany([{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses:["Python", "React", "PHP"],
			department: "none"
		}, {
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:{
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses:["React", "Laravel", "Sass"],
			department: "none"
		}])

		// [Section] Finding Documents (read)
		// Find
		/*
			-If multiple documents match the criteria for finding a document only the first document that matches the search term will be returned.
			-This is based from the order that the documents are stored in a collection

			Syntax:
				db.collectionName.find();
				db.collectionName.find({field: value});
		*/

		// leaving the search criteria empty will retrive all the documents
		db.users.find();

		db.users.find({firstName: "Stephen"});
		// pretty method
		// The pretty method allows us to be able to view the documents returned by our terminals in a better format.
		db.users.find({firstName: "Stephen"}).pretty();

		// finding documents with multiple parameters
		/*
			-Syntax:
				db.collectionName.find({fieldA: valueA,
				fieldB: valueB})
		*/

		db.users.find({lastName: "Armstrong", age: 82}).pretty();

		// [Section] Updating documents

		// Updating a single document
		db.users.insert({
			firstName: "Test",
			lastName: "Test",
			age: 0,
			contact: {
				phone: "00000000",
				email: "test@gmail.com"
			},
			course: [],
			department: "none"
		})

		/*
			-Just like the find method, updateOne method will only manipulate a single document. First document that matches the search criteria will be updated.

			Syntax:
				db.collectionName.updateOne({criteria}, {$set: {field: value}})
		*/

		db.users.updateOne(
			{ firstName: "Bill"},
			{
				$set:{
					firstName: "Gates",
					age: 65,
					contact: {
						phone: "12345678",
						email: "bill@gmail.com"
					},
					courses: ["PHP", "Laravel", "HTML"],
					department: "Operations",
					status: "active"
				}
			}
		);

		// Updating multiple documents
		/*
			db.collectName.updateMany({criteria}, {$set:
			{field: value}})
		*/

		db.users.updateMany({department: "none"}, {
			$set: {
				department: "HR"
			}
		})

		// Replace One
		// Can be used if replacing the whole document necessary
		// Syntax:
		// db.collectionName.replaceOne({criteria}, {$set: {object
//      }})

		db.users.replaceOne({firstName: "Gates"},
		{
					firstName: "Chris",
					lastName: "Mortel",
					age: 14,
					contact: {
						phone: "12345678",
						email: "chris@gmail.com"},
					courses:["PHP", "Laravel", "HTML"],
					department: "Operations"
			}
		)

		// [Section] Deleting documents (delete)

		db.users.insert({
			firstName: "test"
		})

		// Deleting a single document
		/*
			Syntax:
				db.collectionName.deleteOne({criteria});
		*/

		db.users.deleteOne({firstName: "test"});

		// delete Many
		/*
			-Be careful when using the deleteMany method. If no search criteria is provided, it will delete all documents in the collection.
			-DO NOT USE: db.collectionName.deleteMany();
			Syntax:
				db.collectionName.deleteMany({criteria});
		*/

		db.users.deleteMany({firstName: "Chris"});

		// [Section] Advanced queries
		// Query an embedded document

		db.users.find({
			contact: {
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			}
		})

		// Query on nested field
		db.users.find({"contact.phone": "87654321"});

		// Querying an array with exact elements
		db.users.find({courses: ["Python", "React", "PHP"]});

		// Querying an array without regards to orders and elements

		db.users.find({courses: {$all: ["React", "Laravel"]} });